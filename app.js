
function init() {
    var lista = [
        {"nome": "João", "senha": "abc123"},
        {"nome": "Ralf", "senha": "123465"},
        {"nome": "admin", "senha": "951753"}
    ];
    
    
    let [user_a, user_b, admin] = lista;
    admin = {...admin, nome: 'root'};

    console.log(user_a);
    console.log(user_b);
    console.log(admin);
}

init();
